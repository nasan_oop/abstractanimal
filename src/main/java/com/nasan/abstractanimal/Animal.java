/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nasan.abstractanimal;

/**
 *
 * @author nasan
 */
public abstract class Animal {
    private String name;
    private int numberOfLeg;

    public Animal(String name, int numberOfLeg) {
        this.name = name;
        this.numberOfLeg = numberOfLeg;
    }

    public String getName() {
        return name;
    }

    public int getNumberOfLeg() {
        return numberOfLeg;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNumberOfLeg(int numberOfLeg) {
        this.numberOfLeg = numberOfLeg;
    }
    
    @Override
    public String toString(){
        return "Animal{"+"name = "+name+"numberOfLeg = "+numberOfLeg+"}";
    }
    
    public abstract void eat();
    public abstract void walk();
    public abstract void speak();
    public abstract void sleep();
}


