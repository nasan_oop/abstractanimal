/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nasan.abstractanimal;

/**
 *
 * @author nasan
 */
public class Crab extends AquaticAnimal{
    
    private String nickname;

    public Crab(String nickname) {
        super("Crab");
        this.nickname = nickname;
    }
    
    @Override
    public void swim() {
        System.out.println("Crab: " + nickname + " swim");
    }

    @Override
    public void eat() {
        System.out.println("Crab: " + nickname + " eat");
    }

    @Override
    public void walk() {
        System.out.println("Crab: " + nickname + " walk");
    }

    @Override
    public void speak() {
        System.out.println("Crab: " + nickname + " speak");
    }

    @Override
    public void sleep() {
        System.out.println("Crab: " + nickname + " sleep");
    }
    
}
