/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nasan.abstractanimal;

/**
 *
 * @author nasan
 */
public class TestAnimal {
    public static void main(String[] args) {
        Human human = new Human("Ong");
        human.eat();
        human.run();
        human.sleep();
        human.speak();
        human.walk();

        System.out.println("human is Animal ? " + (human instanceof Animal));
        System.out.println("human is LandAnimal ? " + (human instanceof LandAnimal));

        Animal human01 = human;
        System.out.println("human01 is LandAnimal ? " + (human01 instanceof LandAnimal));
        System.out.println("human01 is Reptile ? " + (human01 instanceof Reptile));
        System.out.println("human01 is AquaticAnimal ? " + (human01 instanceof AquaticAnimal));
        System.out.println("human01 is Poultry ? " + (human01 instanceof Poultry));

        Dog dog = new Dog("Dang");
        dog.eat();
        dog.run();
        dog.sleep();
        dog.speak();
        dog.walk();

        System.out.println("dog is Animal ? " + (dog instanceof Animal));
        System.out.println("dog is LandAnimal ? " + (dog instanceof LandAnimal));

        Animal dog01 = dog;
        System.out.println("dog01 is LandAnimal ? " + (dog01 instanceof LandAnimal));
        System.out.println("dog01 is Reptile ? " + (dog01 instanceof Reptile));
        System.out.println("dog01 is AquaticAnimal ? " + (dog01 instanceof AquaticAnimal));
        System.out.println("dog01 is Poultry ? " + (dog01 instanceof Poultry));

        Cat cat = new Cat("Dum");
        cat.eat();
        cat.run();
        cat.sleep();
        cat.speak();
        cat.walk();

        System.out.println("cat is Animal ? " + (cat instanceof Animal));
        System.out.println("cat is LandAnimal ? " + (cat instanceof LandAnimal));

        Animal cat01 = cat;
        System.out.println("cat01 is LandAnimal ? " + (cat01 instanceof LandAnimal));
        System.out.println("cat01 is Reptile ? " + (cat01 instanceof Reptile));
        System.out.println("cat01 is AquaticAnimal ? " + (cat01 instanceof AquaticAnimal));
        System.out.println("cat01 is Poultry ? " + (cat01 instanceof Poultry));

        Crocodile crocodile = new Crocodile("Dee");
        crocodile.eat();
        crocodile.crawl();
        crocodile.sleep();
        crocodile.speak();
        crocodile.walk();

        System.out.println("crocodile is Animal ? " + (crocodile instanceof Animal));
        System.out.println("crocodile is Reptile ? " + (crocodile instanceof Reptile));

        Animal crocodile01 = crocodile;
        System.out.println("crocodile01 is LandAnimal ? " + (crocodile01 instanceof LandAnimal));
        System.out.println("crocodile01 is Reptile ? " + (crocodile01 instanceof Reptile));
        System.out.println("crocodile01 is AquaticAnimal ? " + (crocodile01 instanceof AquaticAnimal));
        System.out.println("crocodile01 is Poultry ? " + (crocodile01 instanceof Poultry));

        Snake snake = new Snake("Dente");
        snake.eat();
        snake.crawl();
        snake.sleep();
        snake.speak();
        snake.walk();

        System.out.println("snake is Animal ? " + (snake instanceof Animal));
        System.out.println("snake is Reptile ? " + (snake instanceof Reptile));

        Animal snake01 = snake;
        System.out.println("snake01 is LandAnimal ? " + (snake01 instanceof LandAnimal));
        System.out.println("snake01 is Reptile ? " + (snake01 instanceof Reptile));
        System.out.println("snake01 is AquaticAnimal ? " + (snake01 instanceof AquaticAnimal));
        System.out.println("snake01 is Poultry ? " + (snake01 instanceof Poultry));

        Fish fish = new Fish("Doremon");
        fish.eat();
        fish.swim();
        fish.sleep();
        fish.speak();
        fish.walk();

        System.out.println("fish is Animal ? " + (fish instanceof Animal));
        System.out.println("fish is AquaticAnimal ? " + (fish instanceof AquaticAnimal));

        Animal fish01 = fish;
        System.out.println("fish01 is LandAnimal ? " + (fish01 instanceof LandAnimal));
        System.out.println("fish01 is Reptile ? " + (fish01 instanceof Reptile));
        System.out.println("fish01 is AquaticAnimal ? " + (fish01 instanceof AquaticAnimal));
        System.out.println("fish01 is Poultry ? " + (fish01 instanceof Poultry));

        Crab crab = new Crab("Dil monas");
        crab.eat();
        crab.swim();
        crab.sleep();
        crab.speak();
        crab.walk();

        System.out.println("crab is Animal ? " + (crab instanceof Animal));
        System.out.println("crab is AquaticAnimal ? " + (crab instanceof AquaticAnimal));

        Animal crab01 = crab;
        System.out.println("crab01 is LandAnimal ? " + (crab01 instanceof LandAnimal));
        System.out.println("crab01 is Reptile ? " + (crab01 instanceof Reptile));
        System.out.println("crab01 is AquaticAnimal ? " + (crab01 instanceof AquaticAnimal));
        System.out.println("crab01 is Poultry ? " + (crab01 instanceof Poultry));

        Bat bat = new Bat("Dana");
        bat.eat();
        bat.fly();
        bat.sleep();
        bat.speak();
        bat.walk();

        System.out.println("bat is Animal ? " + (bat instanceof Animal));
        System.out.println("bat is Poultry ? " + (bat instanceof Poultry));

        Animal bat01 = bat;
        System.out.println("bat01 is LandAnimal ? " + (bat01 instanceof LandAnimal));
        System.out.println("bat01 is Reptile ? " + (bat01 instanceof Reptile));
        System.out.println("bat01 is AquaticAnimal ? " + (bat01 instanceof AquaticAnimal));
        System.out.println("bat01 is Poultry ? " + (bat01 instanceof Poultry));

        Bird bird = new Bird("Dino");
        bird.eat();
        bird.fly();
        bird.sleep();
        bird.speak();
        bird.walk();

        System.out.println("bird is Animal ? " + (bird instanceof Animal));
        System.out.println("bird is Poultry ? " + (bird instanceof Poultry));

        Animal bird01 = bird;
        System.out.println("bird01 is LandAnimal ? " + (bird01 instanceof LandAnimal));
        System.out.println("bird01 is Reptile ? " + (bird01 instanceof Reptile));
        System.out.println("bird01 is AquaticAnimal ? " + (bird01 instanceof AquaticAnimal));
        System.out.println("bird01 is Poultry ? " + (bird01 instanceof Poultry));
    }
}
