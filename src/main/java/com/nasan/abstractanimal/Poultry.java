/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nasan.abstractanimal;

/**
 *
 * @author nasan
 */
public  abstract class Poultry extends Animal {
    
    public Poultry(String name, int numberOfLeg) {
        super(name, numberOfLeg);
    }
    public abstract void fly();
}
