/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nasan.abstractanimal;

/**
 *
 * @author nasan
 */
public abstract class AquaticAnimal  extends Animal{
    
    public AquaticAnimal(String name) {
        super(name,0);
    }
    public abstract void swim();
}
